import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created with IntelliJ IDEA.
 * User: varunshariharan
 * Date: 7/26/12
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class CSToken implements Serializable {
    static int NUMBER_OF_PROCESSES;
    static {
        File configFile = new File("./data/config.txt");
        try {
            List<String> stringList = FileUtils.readLines(configFile);
            NUMBER_OF_PROCESSES = stringList.size();

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    private int tokenValue = 1;
    private int holdingProcess;
    private AtomicIntegerArray currentTimestamp = new AtomicIntegerArray(NUMBER_OF_PROCESSES);
    static final CSToken token = new CSToken();

    @Override
    public String toString(){
        String csTokenString = "";
        csTokenString = String.valueOf(tokenValue)+":"+String.valueOf(holdingProcess)+ ":" +String.valueOf(tokenValue) +":" +currentTimestamp.toString();
        return csTokenString;
    }

    public CSToken read(String input){
        String[] split = input.split(":");
        tokenValue = Integer.parseInt(split[0]);
        holdingProcess = Integer.parseInt(split[1]);
        tokenValue = Integer.parseInt(split[2]);
        String[] arraySplit = split[3].replace("[", "").replace("]", "").split(", ");
        for(int i = 0; i < NUMBER_OF_PROCESSES; i++)
            currentTimestamp.set(i,Integer.parseInt(arraySplit[i]));
        return token;
    }

    public void setHoldingProcess(int holdingProcess) {
        this.holdingProcess = holdingProcess;
    }

    public void setCurrentTimestamp(AtomicIntegerArray currentTimestamp) {
        this.currentTimestamp = currentTimestamp;
    }

    private CSToken(){}
    public void executeCriticalSection(AtomicIntegerArray timestamp, int timer) throws InterruptedException, IOException {
        File file = new File("./data/CS.txt");
        FileWriter fileWriter = new FileWriter(file,true);
        System.out.println("Older timestamp: "+currentTimestamp.toString());
        for(int i = 0; i < NUMBER_OF_PROCESSES; i++){
            currentTimestamp.set(i,timestamp.get(i));
        }
        System.out.print(currentTimestamp.toString() + "\tExecuting in critical section. Token Value = " + (tokenValue++));
        int timerIndex = timer/10;
        while (timerIndex < timer){
            System.out.print(".");
            Thread.sleep(timer/10);
            timerIndex += timer/10;
        }
        String csTextData = "";
        try {
            csTextData = this.currentTimestamp+"\tI(" + this.holdingProcess + ") am currently in critical section\tToken Value = "+(tokenValue-1)+"\n";
            IOUtils.write(csTextData,fileWriter);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (happenedBeforeExists(currentTimestamp,timestamp)){
            String mutexConfirmationText = "Happened before relationship exists between previous and current timestamp.\nMutex successful";
            System.out.println(mutexConfirmationText);
            csTextData += (mutexConfirmationText+"\n");
        }
        System.out.println("Done");
    }

    private boolean happenedBeforeExists(AtomicIntegerArray currentTimestamp, AtomicIntegerArray timestamp) {
        int index = 0;
        while (index < NUMBER_OF_PROCESSES && (currentTimestamp.get(index) != timestamp.get(index))){
            index++;
        }
        if (index != NUMBER_OF_PROCESSES){
            if (currentTimestamp.get(index) < timestamp.get(index))
            {
                for(int i = index+1; i < NUMBER_OF_PROCESSES; i++){
                    if (currentTimestamp.get(i) > timestamp.get(i)){
                        return false;
                    }
                }
            }
            else {
                for(int i = index+1; i < NUMBER_OF_PROCESSES; i++){
                    if (currentTimestamp.get(i) < timestamp.get(i)){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
