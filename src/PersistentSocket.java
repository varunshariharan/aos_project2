import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: varunshariharan
 * Date: 7/26/12
 * Time: 10:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersistentSocket implements Runnable {
    BlockingQueue<Message> queue;
    Socket returnSocket;
    Process process;
    Writer out;
    static int timer;

    static {
        timer = Utility.returnRandomNumber(5000,10000);
    }

    public PersistentSocket(BlockingQueue queue, Socket returnSocket, Process process){
        this.queue = queue;
        this.returnSocket = returnSocket;
        this.process = process;
        try {
            out = new BufferedWriter(new OutputStreamWriter(returnSocket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    @Override
    public void run() {
        while(true){
            Message messageFromQueue = null;
            try {
                messageFromQueue = queue.take();
                messageFromQueue.setTimestamp(process.timestamp);
                messageFromQueue.sendingProcessId = process.processId;
                Utility.sendMessageToProcess(messageFromQueue,returnSocket);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
}