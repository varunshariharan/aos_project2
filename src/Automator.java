import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.*;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.Random;



/**
 * Created with IntelliJ IDEA.
 * User: Anish Srinivas
 * Date: 8/1/12
 * Time: 12:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class Automator {
    public static void main(String[] args) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        File file = new File("./data/config.xml");
        List<Process> processObjectList = Collections.synchronizedList((List<Process>) xStream.fromXML(file));
        int NUMBER_OF_PROCESSES = processObjectList.size();
        Random random = new Random();
        while(true){
            int process = random.nextInt() % NUMBER_OF_PROCESSES;
            process = Math.abs(process);
//            process = 5;
            Process processToSendMessage = processObjectList.get(process);
            Socket processSocket = new Socket(processToSendMessage.ip, processToSendMessage.portNumber);
            System.out.println("Sending Request to "+(process+1));
//            OutputStream outToProcess = processSocket.getOutputStream();
//            ObjectOutputStream out = new ObjectOutputStream(outToProcess);
            PrintStream printStream = new PrintStream(processSocket.getOutputStream());
            Message msg = new Message(Message.CS_REQUEST);
            msg.sendingProcessId = -1;
            printStream.println(msg.toString());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            break;
        }


    }
}
