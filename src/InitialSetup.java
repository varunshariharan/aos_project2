import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Anish Srinivas
 * Date: 7/26/12
 * Time: 11:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class InitialSetup {

    private static int NUMBER_OF_PROCESSES;

    public static void main(String[] args) throws IOException {
        File file = new File("./data/config.txt");
        boolean mutexFlag;

        if (Integer.parseInt(args[0]) == 1)
            mutexFlag = true;
        else
            mutexFlag = false;

        List<String> configLines = FileUtils.readLines(file);
        NUMBER_OF_PROCESSES = configLines.size();
        Process[] processList = new Process[NUMBER_OF_PROCESSES];
        for(int processId = 1; processId <= NUMBER_OF_PROCESSES; processId++ ){
            File serverFile = new File("./data/"+"server"+processId+".txt");
            List<String> strings = FileUtils.readLines(serverFile);
            int index = 0;
            Process tempProcess = new Process(processId);
            for (String line : strings) {
                int temp = Integer.parseInt(line);
                tempProcess.neighbors.add(temp);
                processList[processId - 1] = tempProcess;
            }
            int minValue = 1000;
            if (processId != 1)
            {
                for (int i = 0;i < processList[processId - 1].neighbors.size();i++)
                {
                    if (processList[processId - 1].neighbors.get(i) < minValue)
                    {
                        minValue = processList[processId - 1].neighbors.get(i);
                    }
                }
                processList[processId - 1].tokenRequestProcess = minValue;
            }
        }

        XStream xStream = new XStream(new DomDriver());
        for (int i = 0, configLinesSize = configLines.size(); i < configLinesSize; i++) {
            String configLine = configLines.get(i);
            String[] split = configLine.split(" ");
            int currentServerId = Integer.parseInt(split[0]);
            processList[i].processId = currentServerId;
            processList[i].ip = split[1].split(":")[0];
            processList[i].portNumber = Integer.parseInt(split[1].split(":")[1]);
            processList[i].isExecutingCriticalSection = false;
            if (mutexFlag == true)
                processList[i].hasToken = false;
            else
                processList[i].hasToken = true;

        }
        for (int i = 0, configLinesSize = configLines.size(); i < configLinesSize; i++) {
            Map<Integer,String> processDetails = new ConcurrentHashMap<Integer, String>();
            //TODO: Iterate through neighbors and add all their IP's to processDetails. Do processDetails.put(neighborId, neighborIP);
            //then processList[i].processDetails = processDetails.
            for (Integer neighbor : processList[i].neighbors){
                String address = processList[neighbor - 1].ip + ":" + String.valueOf(processList[neighbor - 1].portNumber);
                processDetails.put(neighbor, address);
            }
            processList[i].processDetails = processDetails;
        }
        processList[0].hasToken = true;
        List<Process> processObjectArrayList = Collections.synchronizedList(new ArrayList<Process>());

        for (Process processObject : processList) {
            processObjectArrayList.add(processObject);
        }

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("./data/config.xml"));
        String xml = xStream.toXML(processObjectArrayList);
        bufferedWriter.write(xml);

        bufferedWriter.close();
    }
}

