import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.io.FileUtils;


/**
 * Created with IntelliJ IDEA.
 * User: varunshariharan
 * Date: 7/24/12
 * Time: 6:41 PM
 * To change this template use File | Settings | File Templates.
 */
//TEST//
public class Process implements Runnable{
    static int NUMBER_OF_PROCESSES;
    static int timer;
    static {
        File configFile = new File("./data/config.txt");
        try {
            List<String> stringList = FileUtils.readLines(configFile);
            NUMBER_OF_PROCESSES = stringList.size();

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        timer = Utility.returnRandomNumber(10000,20000);

    }

    int processId;
    String ip;
    int portNumber;
    ServerSocket serverSocket;
    Map<Integer,String> processDetails = new ConcurrentHashMap<Integer, String>();
    int tokenRequestProcess;
    List<Integer> neighbors = Collections.synchronizedList(new ArrayList<Integer>());
    boolean hasToken, isExecutingCriticalSection, hasPendingRequestInQueue;
    Queue<Integer> requestQueue;
    Map<Integer,PersistentSocket> persistentSocketMap;
    Map<Integer,Socket> outputSocket;
    AtomicIntegerArray timestamp = new AtomicIntegerArray(NUMBER_OF_PROCESSES);

    public Process(Process process){
        //parameters from config file goes to some static field. Read from that static field.
        this.neighbors = Collections.synchronizedList(new ArrayList<Integer>());
        this.processId = process.processId;
        this.tokenRequestProcess = process.tokenRequestProcess;
        this.ip = process.ip;
        this.portNumber = process.portNumber;
        this.hasToken = process.hasToken;
        this.isExecutingCriticalSection = process.isExecutingCriticalSection;
        this.requestQueue = new LinkedList<Integer>();
        this.processDetails = process.processDetails;
        this.persistentSocketMap = new ConcurrentHashMap<Integer, PersistentSocket>();
        this.outputSocket = new ConcurrentHashMap<Integer, Socket>();
        this.hasPendingRequestInQueue = false;

        try {
            serverSocket = new ServerSocket(this.portNumber);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public Process(int processId){
        this.processId = processId;
    }

    public void csEnter(int timer, CSToken token) throws InterruptedException, IOException {
        this.updateTimestamp();
        token.setHoldingProcess(this.processId);
        timestamp.incrementAndGet(processId - 1);
        isExecutingCriticalSection = true;
        token.executeCriticalSection(timestamp,timer);
        csLeave(token);
    }

    public void csLeave(CSToken token) throws InterruptedException, IOException {
        isExecutingCriticalSection = false;
        this.hasPendingRequestInQueue = false;
        //if queue is not empty then send token to process at head of queue
        if(!requestQueue.isEmpty()){
            int processAtHeadOfQueue = requestQueue.remove();
            System.out.println("Removed top of queue: "+this.requestQueue.toString());
            if (processAtHeadOfQueue == this.processId){
                csEnter(timer,token);
            }
            else {

                Message tokenMessage = new Message(Message.TOKEN_MESSAGE,token);
                Socket outputSocket = null;
                this.updateTimestamp();
                tokenMessage.sendingProcessId = this.processId;
                tokenMessage.setTimestamp(this.timestamp);
                {
                    String address = processDetails.get(processAtHeadOfQueue);
                    String[] split = address.split(":");
                    try {
                        outputSocket = new Socket(split[0],Integer.parseInt(split[1]));
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                this.hasToken = false;
                this.hasToken = false;
                this.tokenRequestProcess = processAtHeadOfQueue;
                System.out.println(this.timestamp.toString() + "\tSending token to " + processAtHeadOfQueue + "\tQueue: " + this.requestQueue.toString());
                Utility.sendMessageToProcess(tokenMessage, outputSocket);
                outputSocket.close();
                System.out.println("Checking if the queue is empty"+"\tQueue: "+this.requestQueue.toString());
                if (!this.requestQueue.isEmpty()){
                    //If non empty queue after sending token, also send a request to the process to which token was sent to
                    Thread.sleep(2000);
                    Socket newOutputSocket = null;
                    String address = processDetails.get(tokenRequestProcess);
                    String[] split = address.split(":");
                    try {
                        newOutputSocket = new Socket(split[0],Integer.parseInt(split[1]));
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    Message requestMessage = new Message(Message.CS_REQUEST);
                    this.updateTimestamp();
                    requestMessage.setTimestamp(this.timestamp);
                    requestMessage.sendingProcessId = this.processId;
                    System.out.println(this.timestamp.toString() + "\tSending request to " + processAtHeadOfQueue + "\tQueue: " + this.requestQueue.toString());
                    Utility.sendMessageToProcess(requestMessage, newOutputSocket);
                    newOutputSocket.close();
                }
            }
        }
    }

    public void generateRequest() throws InterruptedException, IOException {
        if(!this.hasPendingRequestInQueue){
            this.hasPendingRequestInQueue = true;
            System.out.println("Generating CS Request"+"\tQueue: "+this.requestQueue.toString());
            if(this.hasToken && !this.isExecutingCriticalSection){
                csEnter(timer, CSToken.token);
            }
            else if (this.hasToken && this.isExecutingCriticalSection){
                this.requestQueue.add(this.processId);
                System.out.println("Request added to queue: "+this.requestQueue.toString());
            }
            else {
                if(this.requestQueue.isEmpty()){
                    requestQueue.add(this.processId);
                    System.out.println("Request added to queue: "+this.requestQueue.toString());
                    if (this.hasToken){
                        requestQueue.remove();
                        System.out.println("Removed top of queue: "+this.requestQueue.toString());
                        CSToken.token.setHoldingProcess(processId);
                        csEnter(timer,CSToken.token);
                    }
                    else {
                        Message requestMessage = new Message(Message.CS_REQUEST);
                        Socket outputSocket = null;
                        {
                            String address = processDetails.get(tokenRequestProcess);
                            String[] split = address.split(":");
                            try {
                                outputSocket = new Socket(split[0],Integer.parseInt(split[1]));
                            } catch (IOException e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                        requestMessage.sendingProcessId = processId;
                        System.out.println(this.timestamp.toString()+"\tSending request to "+tokenRequestProcess+"\tQueue: "+this.requestQueue.toString());
                        updateTimestamp();
                        requestMessage.setTimestamp(timestamp);
                        Utility.sendMessageToProcess(requestMessage,outputSocket);
                        outputSocket.close();
                    }
                }
                else {
                    requestQueue.add(this.processId);
                    System.out.println("Request added to queue: " + this.requestQueue.toString());
                }
            }
        }
        else {
            System.out.println("A request is already present in the request queue. Ignoring request.");
        }
    }

    public static void main(String[] args) {
        //take command line argument - process id
        //TODO read data from config file
        //TODO create process object and initialize it with parameters from config file
        //TODO create automator to randomly generate requests
        XStream xStream = new XStream(new DomDriver());
        File file = new File("./data/config.xml");
        List<Process> processObjectList = Collections.synchronizedList((List<Process>) xStream.fromXML(file));
        Process processObject = new Process(processObjectList.get(Integer.parseInt(args[0])-1));
        Thread mainThread = new Thread(processObject);
        mainThread.start();
    }

    @Override
    public void run() {
        while (true){
            try {
//                System.out.println("Created socket on port : "+this.portNumber);
                Socket returnSocket = serverSocket.accept();
//                System.out.println("Accepted connection from "+returnSocket.getLocalPort());
                ProcessThreadService processThreadService = new ProcessThreadService(returnSocket,this);
                Thread thread = new Thread(processThreadService);
                thread.start();

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }

    public void updateTimestamp(Message messageFromQueue){
        AtomicIntegerArray maxArray = new AtomicIntegerArray(messageFromQueue.timestamp.length());
        for (int i = 0; i < this.timestamp.length(); i++){
            maxArray.set(i,Math.max(this.timestamp.get(i),messageFromQueue.timestamp.get(i)));
        }
        maxArray.incrementAndGet(this.processId-1);
        for (int i = 0; i < this.timestamp.length(); i++){
            this.timestamp.set(i,maxArray.get(i));
        }
    }

    public void updateTimestamp(){
        this.timestamp.incrementAndGet(processId-1);
    }
}
