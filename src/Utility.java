import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: varunshariharan
 * Date: 7/24/12
 * Time: 6:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class Utility {

    private static String configFilePath = "./data/config.txt";

    public static void readConfigFile(){

    }

    public static void createInitialSetup() throws IOException {
        List<String> linesFromConfigFile = FileUtils.readLines(new File(configFilePath));
        Map<Integer,String> serverDetails = new ConcurrentHashMap<Integer, String>();
        for (String line : linesFromConfigFile) {
            String[] lineSplit = line.split(" ");
            int processId = Integer.parseInt(lineSplit[0]);
            String processServerAddress = lineSplit[1];
            serverDetails.put(processId,processServerAddress);
        }

    }

    public static int returnRandomNumber(int min, int max){
        return (min + (int)(Math.random() * ((max - min) + 1)));
    }

    public static void sendMessageToProcess(Message message, Socket processSocket) {
        try {
//            ObjectOutputStream outputStream = new ObjectOutputStream(processSocket.getOutputStream());
//            outputStream.writeObject(message.toString());
            PrintStream printStream = new PrintStream(processSocket.getOutputStream());
            printStream.println(message.toString());
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
