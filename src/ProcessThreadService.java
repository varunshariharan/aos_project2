import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: varunshariharan
 * Date: 7/31/12
 * Time: 1:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProcessThreadService implements Runnable {
    Socket returnSocket;
    Process process;
    static int timer;

    static {
        timer = Utility.returnRandomNumber(5000,10000);
    }

    public ProcessThreadService(Socket returnSocket, Process process) {
        this.returnSocket = returnSocket;
        this.process = process;
    }
    @Override
    public void run() {
        try {
            DataInputStream dataInputStream = new DataInputStream(returnSocket.getInputStream());
            Message message = null;
            int counter = 0;
            String messageString = "";
            while ((messageString = dataInputStream.readLine()) != null){
                    message = new Message(messageString);
                    int sendingProcessId = message.sendingProcessId;
                    process.updateTimestamp();
                    if (sendingProcessId != -1){
                        performActionOnMessageReceipt(message);
                    }
                    else {
                        process.generateRequest();
                    }
            }
        } catch (Exception e) {
            System.out.println("I Shouldn't be here");
            e.printStackTrace();
        }

    }

    private void performActionOnMessageReceipt(Message messageFromQueue) throws InterruptedException, IOException {
        //If CS request message check if you have the token
        int sendingProcessId = messageFromQueue.sendingProcessId;
        String messageString = null;
        if(messageFromQueue.messageType == Message.CS_REQUEST)
            messageString = "CS_REQUEST";
        else if (messageFromQueue.messageType == Message.TOKEN_MESSAGE)
            messageString = "TOKEN";
        System.out.println("Received "+messageString+" from "+sendingProcessId);
        process.updateTimestamp(messageFromQueue);
        // If the sending process ID is -1, then the message has come from an Automator/Client.
        if (messageFromQueue.messageType == Message.CS_REQUEST){
            //If you have token
            if (process.hasToken){
                if (!process.isExecutingCriticalSection){
                    //If token idle then send token message to requesting process
                    Message tokenMessage = new Message(Message.TOKEN_MESSAGE,CSToken.token);
                    System.out.println(process.timestamp.toString()+"\tSending token to "+sendingProcessId+"\tQueue: "+process.requestQueue.toString());
                    //vector clock update for executing message send. Piggybank latest timestamp on message
                    process.updateTimestamp();
                    PersistentSocket persistentSocket = null;
                    if (!process.persistentSocketMap.containsKey(sendingProcessId)){
                        String address = process.processDetails.get(sendingProcessId);
                        String[] split = address.split(":");
                        Socket outputSocket = null;
                        try {
                            outputSocket = new Socket(split[0],Integer.parseInt(split[1]));
                        } catch (IOException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        BlockingQueue queue = new LinkedBlockingQueue();
                        persistentSocket = new PersistentSocket(queue,outputSocket,process);
                        Thread persistentSocketThread = new Thread(persistentSocket);
                        persistentSocketThread.start();
                    }
                    else {
                        persistentSocket = process.persistentSocketMap.get(sendingProcessId);
                    }
                    persistentSocket.queue.put(tokenMessage);
                    process.hasToken = false;
                    process.tokenRequestProcess = sendingProcessId;
                    //change token request process to process that was given the token for future transactions
                }
                else {
                    //Else add the request to the queue
                    process.requestQueue.add(sendingProcessId);
                    System.out.println(process.timestamp.toString() + "\tCurrently executing critical section. Added request to queue: " + process.requestQueue.toString());
                    process.updateTimestamp();
                }
            }
            else {
                //Insert request in queue
                process.updateTimestamp();
                if (process.requestQueue.isEmpty()){
                    process.requestQueue.add(sendingProcessId);
                    System.out.println("Request added to queue: "+process.requestQueue.toString()+"\tQueue: "+process.requestQueue.toString());
                    Message requestMessage = new Message(Message.CS_REQUEST);
                    System.out.println(process.timestamp.toString()+"\tRequesting token from "+process.tokenRequestProcess+"\tQueue: "+process.requestQueue.toString());
                    //vector clock update for executing message send. Piggybank latest timestamp on message
                    process.updateTimestamp();
                    //send request message to next process.
                    PersistentSocket persistentSocket = null;
                    if (!process.persistentSocketMap.containsKey(process.tokenRequestProcess)){
                        String address = process.processDetails.get(process.tokenRequestProcess);
                        String[] split = address.split(":");
                        Socket outputSocket = null;
                        try {
                            outputSocket = new Socket(split[0],Integer.parseInt(split[1]));
                        } catch (IOException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        BlockingQueue queue = new LinkedBlockingQueue();
                        persistentSocket = new PersistentSocket(queue,outputSocket,process);
                        Thread persistentSocketThread = new Thread(persistentSocket);
                        persistentSocketThread.start();
                    }
                    else {
                        persistentSocket = process.persistentSocketMap.get(process.tokenRequestProcess);
                    }
                    persistentSocket.queue.put(requestMessage);
                }
                else {
                    process.requestQueue.add(sendingProcessId);
                    System.out.println("Request added to queue: "+process.requestQueue.toString()+"\tQueue: "+process.requestQueue.toString());
                    process.updateTimestamp();
                }
                //      Send cs_request message to requestTokenProcess if queue is empty

            }
        }
        else if (messageFromQueue.messageType == Message.TOKEN_MESSAGE){
            //look at head of requestQueue.
            messageFromQueue.token.setHoldingProcess(process.processId);
            int processAtHeadOfQueue = process.requestQueue.remove();
            System.out.println("Removed top of queue: "+process.requestQueue.toString());
            process.hasToken = true;
            if(processAtHeadOfQueue == process.processId){
                //If headOfQueue is itself, then execute critical section
                //execute critical section
                try {
                    process.csEnter(timer, messageFromQueue.token);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            else {
                //send token to process at the head of the queue
                Message tokenMessage = new Message(Message.TOKEN_MESSAGE,messageFromQueue.token);
                tokenMessage.sendingProcessId = process.processId;
                process.updateTimestamp();
                System.out.println(process.timestamp.toString()+"\tSending token to "+processAtHeadOfQueue+"\tQueue: "+process.requestQueue.toString());
                //vector clock update for executing message send. Piggybank latest timestamp on message
                tokenMessage.setTimestamp(process.timestamp);
                PersistentSocket persistentSocket = null;
                if (!process.persistentSocketMap.containsKey(processAtHeadOfQueue)){
                    String address = process.processDetails.get(processAtHeadOfQueue);
                    String[] split = address.split(":");
                    Socket outputSocket = null;
                    try {
                        outputSocket = new Socket(split[0],Integer.parseInt(split[1]));
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    BlockingQueue queue = new LinkedBlockingQueue();
                    persistentSocket = new PersistentSocket(queue,outputSocket,process);
                    Thread persistentSocketThread = new Thread(persistentSocket);
                    persistentSocketThread.start();
                }
                else {
                    persistentSocket = process.persistentSocketMap.get(processAtHeadOfQueue);
                }
                process.hasToken = false;
                process.tokenRequestProcess = processAtHeadOfQueue;
                persistentSocket.queue.put(tokenMessage);
                if(!process.requestQueue.isEmpty()){
                    //also send a request if you already have an entry in the queue
                    Message requestMessage = new Message(Message.CS_REQUEST);
                    process.updateTimestamp();
                    System.out.println(process.timestamp.toString()+"\tRequesting token from "+processAtHeadOfQueue+"\tQueue: "+process.requestQueue.toString());
                    //vector clock update for executing message send. Piggybank latest timestamp on message
                    persistentSocket.queue.put(requestMessage);
                }
            }
        }
    }
}
