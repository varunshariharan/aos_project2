import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created with IntelliJ IDEA.
 * User: varunshariharan
 * Date: 7/24/12
 * Time: 6:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class Message implements Serializable {
    public static int CS_REQUEST = 1;
    public static int TOKEN_MESSAGE = 2;
    public static int APPLICATION_MESSAGE = 3;
    static int NUMBER_OF_PROCESSES;
    static {
        File configFile = new File("./data/config.txt");
        try {
            List<String> stringList = FileUtils.readLines(configFile);
            NUMBER_OF_PROCESSES = stringList.size();

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    int messageType; // 1. cs_request 2. token 3. applicationMessage
    int sendingProcessId;
    CSToken token;
    AtomicIntegerArray timestamp = new AtomicIntegerArray(NUMBER_OF_PROCESSES);

    public Message(int messageType){
        this.messageType = messageType;
    }

    public void setTimestamp(AtomicIntegerArray timestamp) {
        for (int i = 0; i < this.timestamp.length(); i++){
            this.timestamp.set(i,timestamp.get(i));
        }
    }

    public Message(int messageType, CSToken token){
        this.messageType = messageType;
        this.token = token;
    }

    public String toString(){
        String messageString = String.valueOf(messageType) + ";" + String.valueOf(sendingProcessId) +";"+timestamp.toString() + ";" + CSToken.token.toString();
        return messageString;
    }

    public Message(String messageString){
        //TODO create constructor from messageString to message. It should use CSToken.read()
        String[] split = messageString.split(";");
        this.messageType = Integer.parseInt(split[0]);
        this.sendingProcessId = Integer.parseInt(split[1]);
        String[] arraySplit = split[2].replace("[", "").replace("]", "").split(", ");
        for(int i = 0; i < NUMBER_OF_PROCESSES; i++)
            timestamp.set(i,Integer.parseInt(arraySplit[i]));
        if (this.messageType == Message.TOKEN_MESSAGE)
        {
            this.token = CSToken.token;
            this.token.read(split[3]);
        }
    }
}
